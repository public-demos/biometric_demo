import 'package:flutter_test/flutter_test.dart';
import 'package:biometric_demo/biometric_demo.dart';
import 'package:biometric_demo/biometric_demo_platform_interface.dart';
import 'package:biometric_demo/biometric_demo_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockBiometricDemoPlatform
    with MockPlatformInterfaceMixin
    implements BiometricDemoPlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');

  @override
  Future<bool?> authenticateWithBiometrics() => Future.value(true);
}

void main() {
  final BiometricDemoPlatform initialPlatform = BiometricDemoPlatform.instance;

  test('$MethodChannelBiometricDemo is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelBiometricDemo>());
  });

  test('getPlatformVersion', () async {
    BiometricDemo biometricDemoPlugin = BiometricDemo();
    MockBiometricDemoPlatform fakePlatform = MockBiometricDemoPlatform();
    BiometricDemoPlatform.instance = fakePlatform;

    expect(await biometricDemoPlugin.getPlatformVersion(), '42');
  });

  test('authenticateWithBiometrics', () async {
    BiometricDemo biometricDemoPlugin = BiometricDemo();
    MockBiometricDemoPlatform fakePlatform = MockBiometricDemoPlatform();
    BiometricDemoPlatform.instance = fakePlatform;

    expect(await biometricDemoPlugin.authenticateWithBiometrics(), true);
  });
}
