
import 'biometric_demo_platform_interface.dart';

class BiometricDemo {
  Future<String?> getPlatformVersion() {
    return BiometricDemoPlatform.instance.getPlatformVersion();
  }

  Future<bool?> authenticateWithBiometrics() {
    return BiometricDemoPlatform.instance.authenticateWithBiometrics();
  }
}
