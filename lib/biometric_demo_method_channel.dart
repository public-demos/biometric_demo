import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'biometric_demo_platform_interface.dart';

/// An implementation of [BiometricDemoPlatform] that uses method channels.
class MethodChannelBiometricDemo extends BiometricDemoPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('biometric_demo');

  @override
  Future<String?> getPlatformVersion() async {
    final version = await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }

  @override
  Future<bool?> authenticateWithBiometrics() async {
    final result = await methodChannel.invokeMethod<bool>('authenticateWithBiometrics');
    return result;
  }
}
