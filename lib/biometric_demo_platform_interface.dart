import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'biometric_demo_method_channel.dart';

abstract class BiometricDemoPlatform extends PlatformInterface {
  /// Constructs a BiometricDemoPlatform.
  BiometricDemoPlatform() : super(token: _token);

  static final Object _token = Object();

  static BiometricDemoPlatform _instance = MethodChannelBiometricDemo();

  /// The default instance of [BiometricDemoPlatform] to use.
  ///
  /// Defaults to [MethodChannelBiometricDemo].
  static BiometricDemoPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [BiometricDemoPlatform] when
  /// they register themselves.
  static set instance(BiometricDemoPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }

  Future<bool?> authenticateWithBiometrics() {
    throw UnimplementedError(
        'authenticateWithBiometrics() has not been implemented.');
  }
}
