import Flutter
import UIKit
import LocalAuthentication

public class SwiftBiometricDemoPlugin: NSObject, FlutterPlugin {
    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "biometric_demo", binaryMessenger: registrar.messenger())
        let instance = SwiftBiometricDemoPlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)
    }
    
    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        
        switch call.method {
        case "getPlatformVersion":
            result("iOS " + UIDevice.current.systemVersion)
        case "authenticateWithBiometrics":
            self.authenticateWithBiometrics(result: result)
        default:
            result(FlutterMethodNotImplemented)
        }
    }

    /**
     * Show a prompt that asks the user to sign in with biometrics (either Face or Fingerprint).
     * If neither is configured by the user on their iOS device, it will prompt for the passcode.
     */
    private func authenticateWithBiometrics(result: @escaping FlutterResult) -> Void  {
        let context = LAContext()
        
        if #available(iOS 10.0, *) {
            context.localizedCancelTitle = "Use Password"
        }
        
        var error: NSError?
        
        // Check for biometric authentication permissions
        let permissions = context.canEvaluatePolicy(
            .deviceOwnerAuthentication,
            error: &error
        )
        
        if permissions {
            let reason = "Log in with Face ID"
            context.evaluatePolicy(
                // .deviceOwnerAuthentication allows biometric or passcode authentication
                .deviceOwnerAuthentication,
                localizedReason: reason
            ) { success, error in
                // Send the authentication result to Flutter, either true or false.
                result(success)
            }
        } else {
            // If the biometrics permissions failed, throw a PlatformException to Flutter.
            let platformError = FlutterError(code: "AUTHFAILED",
                                             message: "\(error?.localizedDescription ?? "The authentication process has failed for an unknown reason").",
                                             details: nil)
            result(platformError)
        }
        
        return
    }
}
