#import "BiometricDemoPlugin.h"
#if __has_include(<biometric_demo/biometric_demo-Swift.h>)
#import <biometric_demo/biometric_demo-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "biometric_demo-Swift.h"
#endif

@implementation BiometricDemoPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftBiometricDemoPlugin registerWithRegistrar:registrar];
}
@end
