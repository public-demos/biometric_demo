import 'dart:async';

import 'package:biometric_demo/biometric_demo.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';
  AuthStatus _authStatus = AuthStatus.idle;
  String? _error;

  final _biometricDemoPlugin = BiometricDemo();

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    // We also handle the message potentially returning null.
    try {
      platformVersion =
          await _biometricDemoPlugin.getPlatformVersion() ?? 'Unknown platform version';
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  Future<void> authenticateWithBiometrics() async {
    AuthStatus authStatus = AuthStatus.idle;
    String? error;

    try {
      final result = await BiometricDemo().authenticateWithBiometrics() ??
          false;
      authStatus = result ? AuthStatus.success : AuthStatus.failed;
    } on PlatformException catch (e) {
      authStatus = AuthStatus.failed;
      error = e.message;
    }

    if (mounted)
      setState(() {
        this._authStatus = authStatus;
        this._error = error;
      });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Running on: $_platformVersion\n'),
              SizedBox(height: 50),
              Chip(
                label: Text(
                  _authStatus.value.text,
                  style: const TextStyle(color: Colors.white),
                ),
                backgroundColor: _authStatus.value.color,
              ),
              TextButton(
                onPressed: authenticateWithBiometrics,
                child: const Text("Sign in"),
              ),
              if (_error != null) Text(_error!)
            ],
          ),
        ),
      ),
    );
  }
}

class AuthStatusItem {
  const AuthStatusItem({
    required this.color,
    required this.text,
  });

  final Color color;
  final String text;
}

enum AuthStatus {
  idle(AuthStatusItem(color: Colors.blueGrey, text: 'Not started')),
  success(AuthStatusItem(color: Colors.green, text: 'Success')),
  failed(AuthStatusItem(color: Colors.red, text: 'Failure'));

  final AuthStatusItem value;

  const AuthStatus(this.value);
}
